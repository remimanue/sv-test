'use strict';

var moment = require('moment-timezone');
var fs = require('fs');
var _ = require('lodash');

exports.convertDateObjectToUtc = function convertDateObjectToUtc(date) {
  return moment(date).tz('Etc/Universal');
};

exports.getCurrentDateAsUtc = function getCurrentDateAsUtc() {
  var now = new Date();
  return new Date(
    now.getUTCFullYear(),
    now.getUTCMonth(),
    now.getUTCDate(),
    now.getUTCHours(),
    now.getUTCMinutes(),
    now.getUTCSeconds()
  );
};

exports.getTimezones = function getTimezones() {
  var timezoneData = JSON.parse(fs.readFileSync('node_modules/moment-timezone/data/unpacked/latest.json', 'utf8'));
  var timezones = [];
  _.forEach(timezoneData.zones, function iterator(value, key) {
    timezones.push({ id: key, name: value });
  });
  return _.uniq(_.sortBy(timezones, 'name'), false, function iterator(timezone) {
    return timezone.name;
  });
};
