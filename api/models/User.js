'use strict';

var _ = require('lodash');

module.exports = _.merge(_.cloneDeep(require('../base/Model')), {
  attributes: {
    username: {
      type: 'string',
      unique: true
    },
    email: {
      type: 'email',
      unique: true
    },
    firstName: {
      type: 'string',
      required: true
    },
    lastName: {
      type: 'string',
      required: true
    },
    admin: {
      type: 'boolean',
      defaultsTo: false
    },

    // Passport configurations
    passports: {
      collection: 'Passport',
      via: 'user'
    },
    // Message objects that user has sent
    messages: {
      collection: 'Message',
      via: 'user'
    },
    // Login objects that are attached to user
    logins: {
      collection: 'UserLogin',
      via: 'user'
    },
    requestLogs: {
      collection: 'RequestLog',
      via: 'user'
    },

	 // generic 'createdUser' and 'updatedUser' properties
    // Utilisateurs
    createdUtilisateurs: {
      collection: 'Utilisateur',
      via: 'createdUser'
    },
    updatedUtilisateurs: {
      collection: 'Utilisateur',
      via: 'updatedUser'
    },
    // Documents
    createdDocuments: {
      collection: 'Document',
      via: 'createdUser'
    },
    updatedDocuments: {
      collection: 'Document',
      via: 'updatedUser'
    }
  }
});
