'use strict';

var _ = require('lodash');

module.exports = _.merge(_.cloneDeep(require('../base/Model')), {
  attributes: {
    name: {
      type: 'string',
      required: true
    },
    description: {
      type: 'text'
    },

    documents: {
      collection: 'document',
      via: 'utilisateur'
    }
  }
});
