'use strict';

module.exports = {
  schema: true,

  attributes: {
    ip: {
      type: 'string',
      required: true
    },
    host: {
      type: 'string',
      required: true
    },
    agent: {
      type: 'text',
      required: true
    },
    browser: {
      type: 'string',
      defaultsTo: 'Unknown'
    },
    browserVersion: {
      type: 'string',
      defaultsTo: 'Unknown'
    },
    browserFamily: {
      type: 'string',
      defaultsTo: 'Unknown'
    },
    os: {
      type: 'string',
      defaultsTo: 'Unknown'
    },
    osVersion: {
      type: 'string',
      defaultsTo: 'Unknown'
    },
    osFamily: {
      type: 'string',
      defaultsTo: 'Unknown'
    },
    count: {
      type: 'integer',
      defaultsTo: 1
    },

    // Attached User object of this UserLogin
    user: {
      model: 'User',
      columnName: 'userId',
      required: true
    }
  }
};
