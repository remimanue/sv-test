'use strict';

module.exports = {
  schema: true,

  attributes: {
    method: {
      type: 'string',
      required: true
    },
    url: {
      type: 'string',
      required: true
    },
    headers: {
      type: 'json'
    },
    parameters: {
      type: 'json'
    },
    query: {
      type: 'json'
    },
    body: {
      type: 'json'
    },
    protocol: {
      type: 'string'
    },
    ip: {
      type: 'string'
    },
    responseTime: {
      type: 'integer'
    },
    middlewareLatency: {
      type: 'integer'
    },

    // User object
    user: {
      model: 'User',
      columnName: 'userId',
      required: true
    }
  }
};
