'use strict';

var _ = require('lodash');

module.exports = _.merge(_.cloneDeep(require('../base/Model')), {
  attributes: {
    title: {
      type: 'string',
      required: true
    },
    description: {
      type: 'text',
      defaultsTo: ''
    },
    releaseDate: {
      type: 'date',
      required: true
    },

    // Utilisateur TODO type client
    utilisateur: {
      model: 'utilisateur'
    }
  }
});
