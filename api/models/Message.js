'use strict';

module.exports = {
  schema: true,

  attributes: {
    nick: {
      type: 'text',
      required: true
    },
    message: {
      type: 'text',
      required: true
    },

    user: {
      model: 'user'
    }
  }
};
