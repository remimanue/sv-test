'use strict';

var _ = require('lodash');
var Promise = require('bluebird');

module.exports = _.merge(_.cloneDeep(require('../base/Controller')), {
  'statistics': function statistics(request, response) {
    var type = request.param('type');
    var groupBy;

    switch (type) {
      case 'Browser':
        groupBy = 'browser';
        break;
      case 'OS':
        groupBy = 'os';
        break;
      case 'User':
      default:
        groupBy = 'userId';
        break;
    }

    var fetchStatistics = function fetchStatistics() {
      return sails.models['userlogin']
          .find().sum('count')
          .groupBy(groupBy)
      ;
    };

    var fetchUsers = function fetchUsers() {
      return (groupBy === 'userId') ? sails.models.user.find() : [];
    };

    var formatData = function formatData(data) {
      return _.map(data.stats, function iterator(item) {
        return [
          (groupBy === 'userId') ? _findUser(item['user']) : item[groupBy],
          item.count
        ];
      });

      function _findUser(userId) {
        var user = _.find(data.users, function iterator(user) {
          return user.id === userId;
        });
        return user ? user.lastName + ', ' + user.firstName + ' (' + user.username + ')' : 'Unknown user';
      }
    };

    var handlerSuccess = function handlerSuccess(data) {
      response.ok(data);
    };

    var handlerError = function handlerError(error) {
      response.negotiate(error);
    };

    // Fetch data and do the all necessary tricks :D
    Promise
      .props({
        stats: fetchStatistics(),
        users: fetchUsers()
      })
      .then(formatData)
      .then(handlerSuccess)
      .catch(handlerError)
    ;
  }
});
